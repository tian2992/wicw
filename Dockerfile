FROM python:3.5-onbuild
# TODO: Migrate to not -onbuild
ENV PYTHONUNBUFFERED 1
RUN python3 manage.py collectstatic --noinput
RUN python3 manage.py makemigrations
from django.db import models

# Create your models here.


class Category(models.Model):
    title = models.CharField(max_length=100, db_index=True)
    slug = models.SlugField(max_length=100, db_index=True)

    def __str__(self):
        return self.title


class NewsItem(models.Model):
    title = models.CharField(max_length=100, unique=True)
    slug = models.SlugField(max_length=100, unique=True)
    body = models.TextField()
    posted = models.DateField(db_index=True, auto_now_add=True)
    featured = models.BooleanField(default=False)

    featured_image = models.ImageField(null=True)

    category = models.ForeignKey(Category)
    chapter = models.ForeignKey('CoffeeChapter', null=True)

    def __str__(self):
        return self.title


class CoffeeChapter(models.Model):
    """
    Chapter
    Address
    Company name
    Phone
    City and country
    Email
    """

    name = models.CharField(max_length=100, unique=True)
    slug = models.SlugField(max_length=50, unique=True)

    logo = models.ImageField()
    thumb_image = models.ImageField()
    featured_image = models.ImageField()
    header_image = models.ImageField()

    buy_banner_image = models.ImageField(null=True)
    # More images on Chapter Image

    description = models.TextField()

    field_1_title = models.CharField(max_length=80, default="History", null=True)
    field_1 = models.TextField(blank=True, null=True)
    field_2_title = models.CharField(max_length=80, default="Coffee Info", null=True)
    field_2 = models.TextField(blank=True, null=True)
    field_3_title = models.CharField(max_length=80, blank=True, null=True)
    field_3 = models.TextField(blank=True, null=True)
    field_4_title = models.CharField(max_length=80, blank=True, null=True)
    field_4 = models.TextField(blank=True, null=True)

    def __str__(self):
        return u"Chapter: {}".format(self.name)


class CoffeeProducer(models.Model):
    name = models.CharField(max_length=50, db_index=True)
    slug = models.SlugField(max_length=50, db_index=True)
    address = models.CharField(max_length=100, blank=True, null=True)

    chapter = models.ForeignKey(CoffeeChapter)

    long = models.FloatField(blank=True, null=True)
    lat = models.FloatField(blank=True, null=True)

    def __str__(self):
        return self.name


class ChapterImage(models.Model):
    image = models.ImageField()
    name = models.CharField(max_length=100, null=True)
    caption = models.TextField(blank=True, null=True)

    chapter = models.ForeignKey(CoffeeChapter)

    def __str__(self):
        return u"Image {}: {}".format(self.name, self.caption)


class ChapterContact(models.Model):
    name = models.CharField(max_length=100, unique=True)
    email = models.EmailField()
    telephone = models.CharField(max_length=20)

    chapter = models.ForeignKey(CoffeeChapter)

    def __str__(self):
        return self.name


class ProducerSellerContact(models.Model):
    name = models.CharField(max_length=100, unique=True)
    email = models.EmailField()
    telephone = models.CharField(max_length=45)

    extra_info = models.CharField(max_length=250, blank=True, null=True)

    chapter = models.ForeignKey(CoffeeProducer)

    def __str__(self):
        return self.name


class CoffeeVariety(models.Model):
    name = models.CharField(max_length=35)
    coffee_variety = models.CharField(max_length=2500)
    coffee_attribute_description = models.TextField()

    annual_production = models.TextField(max_length=140, blank=True, null=True)

    altitude = models.TextField(max_length=15, blank=True, null=True)

    long = models.FloatField(blank=True, null=True)
    lat = models.FloatField(blank=True, null=True)

    rainfall = models.TextField(max_length=15, blank=True, null=True)
    temperature = models.TextField(max_length=15, blank=True, null=True)

    soil_type = models.TextField(max_length=500, blank=True, null=True)

    process = models.TextField(max_length=500, blank=True, null=True)
    harvest_season = models.TextField(max_length=35, blank=True, null=True)

    other_crops = models.TextField(max_length=500, blank=True,  null=True)

    certifications = models.TextField(max_length=140, blank=True, null=True)
    extra_info = models.TextField(blank=True, null=True)

    # A Producer, associated to a Chapter, can have, many Varieties
    producer = models.ForeignKey(CoffeeProducer)

    def __str__(self):
        return self.name


class CoffeeEvent(models.Model):

    name = models.CharField(max_length=250)
    start_date = models.DateField()
    dates = models.CharField(max_length=100)
    venue = models.CharField(max_length=150, null=True)
    theme = models.CharField(max_length=100, blank=True, null=True)
    event_featured_picture = models.ImageField(null=True)

    details = models.TextField()

    event_url = models.CharField(max_length=140, blank=True, null=True)

    featured = models.BooleanField(default=False)

    def __str__(self):
        return self.name


class CoffeeCatalog(models.Model):
    posted_date = models.DateField(db_index=True, auto_now_add=True)
    file_data = models.FileField()

    def __str__(self):
        return u"catalog uploaded at: {}".format(str(self.posted_date))

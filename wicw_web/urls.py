from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static

from . import views

urlpatterns = [

    # URLs of the type "/prod/cafe123"
    url(r'^chapter/(?P<slug>[^\.]+)',
        views.view_chapter,
        ),

    url(r'^chapter/',
        views.view_chapter_list),

    url(r'^chapters/',
        views.view_chapter_list),

    url(r'^news/(?P<chapter_slug>[^\.]+)',
        views.view_news_chapter),

    url(r'^news/',
        views.view_news),

    url(r'^post/(?P<slug>[^\.]+)',
        views.view_post),

    url(r'^category/(?P<slug>[^\.]+)',
        views.view_category),

    url(r'^events/',
        views.view_event_list),

    url(r'^event/(?P<ids>[^\.]+)',
        views.view_event),

    url(r'^$',
        views.index
        )
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

from django.shortcuts import render
from .models import Category, NewsItem, CoffeeChapter, CoffeeVariety, \
    CoffeeProducer, ChapterImage, ChapterContact, ProducerSellerContact, \
    CoffeeEvent, CoffeeCatalog

# Create your views here.
from django.shortcuts import render_to_response, get_object_or_404


def index(request):
    cats = Category.objects.all()
    feat_post = NewsItem.objects.filter(featured=True).order_by('-posted')[::2]
    chapters = CoffeeChapter.objects.all()
    catalog_obj = CoffeeCatalog.objects.order_by('-posted_date').first()
    feat_event = CoffeeEvent.objects.filter(featured=True).order_by('-start_date').first()
    return render_to_response('index.html', {
        'categories': cats,
        'featured_posts': feat_post,
        'featured_event': feat_event,
        'chapters': chapters,
        'catalog': catalog_obj
    })


def view_news(request):
    cats = Category.objects.all()
    blags = NewsItem.objects.all()[:25]
    return render_to_response('news.html', {
        'categories': cats,
        'posts': blags
    })


def view_news_chapter(request, chapter_slug):
    cats = Category.objects.all()
    chap = CoffeeChapter.objects.filter(slug=chapter_slug).first()
    blags = NewsItem.objects.filter(chapter=chap)
    return render_to_response('news.html', {
        'categories': cats,
        'chapter': chap,
        'posts': blags
    })


def view_chapter(request, slug):
    chapter = get_object_or_404(CoffeeChapter, slug=slug)
    producers_list = CoffeeProducer.objects.filter(chapter=chapter)
    coffee_varieties = CoffeeVariety.objects.filter(producer__chapter=chapter)
    coffee_vars = []
    for variety in coffee_varieties:
        pcontact = ProducerSellerContact.objects.filter(chapter=variety.producer).first()
        coffee_vars.append((variety, pcontact))
    header_pictures = list(ChapterImage.objects
                           .filter(name__startswith="menu_thumb_", chapter=chapter)
                           .order_by('name'))
    chapter_pictures = list(ChapterImage.objects
                            .filter(name__startswith="chapter_image_", chapter=chapter)
                            .order_by('name'))
    chapter_contacts = ChapterContact.objects.filter(chapter=chapter)
    producer_contacts = ProducerSellerContact.objects.filter(chapter__chapter=chapter)
    return render_to_response('view_chapter.html', {
        'chapter': chapter,
        'producers_list': producers_list,
        'varieties': coffee_vars,
        'header_pictures': header_pictures,
        'chapter_pictures': chapter_pictures,
        'chapter_contacts': chapter_contacts,
        'producer_contacts': producer_contacts
    })


def view_chapter_list(request):
    chapters = CoffeeChapter.objects.order_by('name').all()
    catalog_obj = CoffeeCatalog.objects.order_by('-posted_date').first()
    return render_to_response('view_chapter_list.html', {
        'chapters': chapters,
        'catalog': catalog_obj
    })


def view_event_list(request):
    return render_to_response('events_list.html', {
        'event_list': CoffeeEvent.objects.all()[:20],
        'chapters': CoffeeChapter.objects.all()[:4]
    })


def view_event(request, ids):
    return render_to_response('view_event.html', {
        'event': get_object_or_404(CoffeeEvent, id=ids),
        'chapters': CoffeeChapter.objects.all()[:4]
    })


def view_post(request, slug):
    return render_to_response('view_post.html', {
        'post': get_object_or_404(NewsItem, slug=slug),
        'chapters': CoffeeChapter.objects.all()[:4]
    })


def view_category(request, slug):
    category = get_object_or_404(Category, slug=slug)
    return render_to_response('view_category.html', {
        'category': category,
        'posts': NewsItem.objects.filter(category=category)[:5]
    })

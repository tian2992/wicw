from django.contrib import admin

# Register your models here.

from .models import *


class BlogAdmin(admin.ModelAdmin):
    exclude = ['posted']
    prepopulated_fields = {'slug': ('title',)}


class CategoryAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('title',)}


class CoffeeChapterAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('name',)}


class CoffeeProducerAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('name',)}


class CoffeeVarietyAdmin(admin.ModelAdmin):
    prepopulated_fields = {'name': ('coffee_variety',)}


admin.site.register(NewsItem, BlogAdmin)
admin.site.register(Category, CategoryAdmin)

admin.site.register(CoffeeChapter, CoffeeChapterAdmin)
admin.site.register(CoffeeProducer, CoffeeProducerAdmin)
admin.site.register(CoffeeVariety, CoffeeVarietyAdmin)

admin.site.register(ChapterContact)
admin.site.register(ChapterImage)
admin.site.register(ProducerSellerContact)
admin.site.register(CoffeeEvent)
admin.site.register(CoffeeCatalog)
